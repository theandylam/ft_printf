# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alam <theandylam@gmail.com>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/03 17:01:47 by alam              #+#    #+#              #
#    Updated: 2016/11/03 17:01:49 by alam             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

PRINTF_SRC = 	ft_printf.c conversion_analyze.c number_functions.c get_mods.c \
				print_string.c parse_argument.c number_output_handler.c \
				number_functions2.c string_output_handler.c print_number.c

LIBFT_SRC = 	ft_atoi.c ft_isdigit.c ft_itoa_base.c ft_memalloc.c ft_putchar.c \
				ft_putstr.c ft_putwchar.c ft_putwstr.c ft_strchr.c ft_strlcat.c \
				ft_strlen.c ft_strlower.c ft_strncpy.c ft_isupper.c ft_tolower.c \
				ft_putnstr.c ft_putnwstr.c ft_wstrlen.c ft_strcmp.c \
				ft_itoa_base_u.c ft_wstrsize.c ft_wstrnsize.c

OBJECTS = $(addprefix libft/, $(LIBFT_SRC:.c=.o)) $(PRINTF_SRC:.c=.o)

CFLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJECTS)
	@ar rc $(NAME) $(OBJECTS)
	@ranlib $(NAME)

%.o: %.c
	gcc $(CFLAGS) -o $@ -c $^

clean:
	rm -f $(OBJECTS)

fclean: clean
	rm -f $(NAME)

re: fclean all

copy:
	rm -rf libft/
	mkdir libft
	cp $(addprefix ~/libft/, $(LIBFT_SRC)) ~/libft/libft.h ./libft/

.PHONY: all clean fclean re
